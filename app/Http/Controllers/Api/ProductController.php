<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Eloquent\Product;
use App\Services\IPFSService;

use RpcClient;

class ProductController extends Controller
{
    protected $ipfsService;

    public function __construct(IPFSService $ipfsService)
    {
        $this->ipfsService = $ipfsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::orderBy('created_at', 'desc')->paginate(15);
        $response = [
            'status' => 200,
            'success' => true,
            'data' => $product,
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['detail'] = $request->detail;
        $data['code'] = $request->code;
        $image = $request->file('image');
        
        if (!is_null($image)) {
            $image_res = $this->ipfsService->store($image);

            $data['image'] = [
                'provider' => 'ipfs',
                'hash' => $image_res['Hash'],
                'md5' => $image_res['md5'],
            ];
        }

        // 將 data 轉換成 json 格式，再轉換成 base64 字串，再轉換成 hex 字串
        $data = $this->str2hex(base64_encode(json_encode($data)));

        $eth_params = [
            [
                'from' => '0x004ec07d2329997267Ec62b4166639513386F32E',
                'to' => '0x00Bd138aBD70e2F00903268F3Db08f2D25677C9e',
                'value' => '0xde0b6b3a7640000',
                'data' => "0x$data",
            ],
            "user",
        ];

        $eth_res = RpcClient::personal_sendTransaction($eth_params);

        $product = new Product;
        $product->code = $request->code;
        $product->eth_hash = $eth_res;
        $product->save();

        $response = [
            'status' => 200,
            'success' => true,
            'data' => $product,
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where("code", $id)->first();

        $eth_params = [ $product->eth_hash ];
        $eth_res = RpcClient::eth_getTransactionByHash($eth_params);
        $eth_res['input_raw'] = json_decode(base64_decode(
          $this->hex2str(
            str_replace('0x', '', $eth_res['input'])
          )
        ));

        $response = [
            'status' => 200,
            'success' => true,
            'data' => $eth_res,
        ];

        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function str2hex($string)
    {
        $string = str_split($string);
        foreach($string as &$char)
            $char = dechex(ord($char));
        return implode('',$string);
    }

    private function hex2str($hex){
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }
}
