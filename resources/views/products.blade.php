@extends('layouts.app')

@section('content')
<div class="container">
    <h3>產品列表</h3>
    <div id="list-product" class="list-group">
        載入中...
    </div>
</div>

<div id="detail-product" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">載入中...</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>

<div id="detail-product-qrcode" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
$(function () {
    GET_listProduct(1, true);
});

$("#detail-product").on("shown.bs.modal", function (event) {
    var $button = $(event.relatedTarget);
    var id = $button.data('id');
    
    GET_detailProduct(id);
});

$("#detail-product").on("hidden.bs.modal", function (event) {
    var $target = $(event.currentTarget)
    $target.find(".modal-title").html("載入中...")
    $target.find(".modal-body").html("")
});

$("#detail-product-qrcode").on("shown.bs.modal", function (event) {
    var $target = $(event.currentTarget)
    var $button = $(event.relatedTarget)
    var id = $button.data("id")
    $target.find(".modal-body").html(`<img style="width: 100%" src="/qrcode/${id}" alt=""/>`)
});

$("#detail-product-qrcode").on("hidden.bs.modal", function (event) {
    var $target = $(event.currentTarget)

    $target.find(".modal-body").html("")
});


function GET_listProduct(page, init) {
    axios.get('/api/product', {
        params: {
            page: page || 1,
        }
    })
    .then(res => {
        var $target = $("#list-product");
        if (init === true) {
            $target.html('');
        }

        _.map(res.data.data.data, function (data) {
            var item = `<a class="list-group-item">
                <div class="row">
                    <div class="col-xs-8" data-toggle="modal" data-target="#detail-product" data-id="${data.code}">
                        <h4 class="list-group-item-heading">${data.code}</h4>
                        <p class="list-group-item-text" style="word-wrap: break-word;">${data.eth_hash}</p>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#detail-product-qrcode" data-id="${data.code}">
                            <span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                
            </a>`

            $target.append(item);
        });
    })
}

function GET_detailProduct(id) {
    axios.get('/api/product/' + id)
    .then(res => {
        var data = _.get(res, 'data.data', {});
        var product = _.get(res, 'data.data.input_raw', {});
        var image = _.get(res, 'data.data.input_raw.image', {});
        var $modal = $("#detail-product");

        $modal.find(".modal-title").html("產品 " + id);
        
        $modal.find(".modal-body").html(`
        <div>
            <h4>基本訊息</h4>
                Ethereum 識別碼：<span style="word-wrap: break-word;">${_.get(data, 'hash', '')}</span><br>
            <hr>
            <h4>商品</h4>
            品名：<span>${product.name}</span><br>
            識別碼：<span style="word-wrap: break-word;">${product.code}</span><br>
            描述：<span>${product.detail}</span><br>
            <hr>
            <h4>附件</h4>
            MD5：<span>${image.md5}</span><br>
            <img style="width: 100%" src="https://ipfs.io/ipfs/${image.hash}" alt=""/>
        </div>
        `);
    });
}
</script>
@endsection