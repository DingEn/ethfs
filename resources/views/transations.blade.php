@extends('layouts.app')

@section('content')
<div class="container">
    <h3>交易列表</h3>
    <div id="list-transation" class="list-group">
        載入中...
    </div>
</div>

<div id="detail-transation" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">載入中...</h4>
            </div>
            <div class="modal-body">
                <div class="transation-data"></div>
                <div class="transation-attach"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
            </div>
        </div>
    </div>
</div>

<div id="detail-transation-qrcode" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(function () {
    moment.locale('zh-hk')
    GET_listTransation(1, true);
})

$("#detail-transation").on("shown.bs.modal", function (event) {
    var $button = $(event.relatedTarget)
    var id = $button.data('id')
    
    GET_detailTransation(id)
})

$("#detail-transation").on("hidden.bs.modal", function (event) {
    var $target = $(event.currentTarget)
    $target.find(".modal-title").html("載入中...")
    $target.find(".transation-data").html("")
    $target.find(".transation-attach").html("")
})

$("#detail-transation-qrcode").on("shown.bs.modal", function (event) {
    var $target = $(event.currentTarget)
    var $button = $(event.relatedTarget)
    var id = $button.data("id")
    $target.find(".modal-body").html(`<img style="width: 100%" src="/qrcode/${id}" alt=""/>`)
})

$("#detail-transation-qrcode").on("hidden.bs.modal", function (event) {
    var $target = $(event.currentTarget)

    $target.find(".modal-body").html("")
})

function GET_listTransation(page, init) {
    axios.get('/api/transation', {
        params: {
            page: page || 1,
        }
    })
    .then(res => {
        if (init === true) {
            $("#list-transation").html('')
        }

        _.map(res.data.data.data, function (data) {
            var item = `<a class="list-group-item">
                <div class="row">
                    <div class="col-xs-8" data-toggle="modal" data-target="#detail-transation" data-id="${data.id}">
                        <h4 class="list-group-item-heading">${data.id}</h4>
                        <p class="list-group-item-text" style="word-wrap: break-word;">${data.eth_hash}</p>
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#detail-transation-qrcode" data-id="${data.id}">
                            <span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                
            </a>`

            $("#list-transation").append(item);
        })
    })
}

function GET_detailTransation(id) {
    axios.get('/api/transation/' + id)
    .then(res => {
        var data = _.get(res, 'data.data', {})

        var order_data = _.get(res, 'data.data.input_raw.source', {})
        var attach_data = _.get(res, 'data.data.input_raw.attachs', [])
        var $modal = $("#detail-transation")

        $modal.find(".modal-title").html("交易 " + id)
        $modal.find(".transation-data").html(`
        <div>
            <h4>基本訊息</h4>
                識別碼：<span style="word-wrap: break-word;">${_.get(data, 'hash', '')}</span><br>
                交易時間：<span>${moment.unix(parseInt(_.get(order_data, 'timestamp', 0))).format('lll')}</span>
            <hr>
            <h4>賣家</h4>
                名稱：<span>${_.get(order_data, 'product_groups[0].name', '')}</span><br>
            <hr>
            <h4>買家</h4>
                名稱：<span>${_.get(order_data, 'firstname', '')} ${_.get(order_data, 'lastname', '')}</span><br>
                信箱：<span>${_.get(order_data, 'email', '')}</span>
            <hr>
            <h4>商品</h4>
            ${
                _.map(_.get(order_data, 'products', []), function(product) {
                    
                    return `<div>
                        品名：<span>${product.product}</span><br>
                        數量：<span>${product.amount}</span><br>
                        價格：<span>${product.price}</span><br>
                    </div>`
                })
            }
            <hr>
            <h4>附件</h4>
            ${
                _.map(attach_data, function(attach) {
                    return `<div>
                        <img style="width: 100%" src="https://ipfs.io/ipfs/${attach.hash}" alt=""/>
                    </div>
                    `
                })
            }
        </div>
        `)
        $modal.find(".transation-attach").html()
    })
}
</script>
@endsection
