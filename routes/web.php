<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    // return view('welcome');
    return redirect('/products');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/transations', function () {
    return view('transations');
});

Route::get('/products', function () {
    return view('products');
});

Route::resource('qrcode', 'QRCodeController', ['only' => ['show']]);
