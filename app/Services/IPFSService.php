<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Curl;

class IPFSService 
{
    private $url;

    public function __construct()
    {
         $this->url = env('IPFS_API_URL', '');
    }

    public function store(UploadedFile $file)
    {
        $api = '/api/v0/add?recursive=false';

        if (!empty($file)) {
            $file_res = Curl::to($this->url."".$api)
                ->withData([
                    'file' => curl_file_create($file->path(), null, $file->hashName()),
                ])
                ->containsFile()
                ->post();
            if (is_null($file_res)) throw new Exception("Upload to IPFS error");

            $file_res = json_decode($file_res, true);
            if (!is_array($file_res)) throw new Exception("JSON parse error");

            $md5 = md5_file($file->path());

            return array_merge($file_res, [ 'md5' => $md5 ]);
        }
    }
}