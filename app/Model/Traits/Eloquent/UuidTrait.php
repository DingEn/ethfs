<?php
namespace App\Model\Traits\Eloquent;

use Webpatser\Uuid\Uuid;
use Log;

trait UuidTrait
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}