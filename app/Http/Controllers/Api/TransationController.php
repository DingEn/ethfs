<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Eloquent\Transation;
use App\Services\IPFSService;

use RpcClient;
use Curl;

class TransationController extends Controller
{
    protected $ipfsService;

    public function __construct(IPFSService $ipfsService)
    {
        $this->ipfsService = $ipfsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transation = Transation::paginate(15);
        $response = [
            'status' => 200,
            'success' => true,
            'data' => $transation,
        ];

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $data['source'] = $this->parseJsonOrString($request->data);
        $data['attachs'] = $request->file('attachs');

        // 上傳附件到IPFS
        if (!is_null($data['attachs'])) {
            foreach ($data['attachs'] as $key => $attach) {
                $attach_res = $this->ipfsService->store($attach);

                $data['attachs'][$key] = [
                    'provider' => 'ipfs',
                    'hash' => $attach_res['Hash'],
                    'md5' => $attach_res['md5'],
                ];
            }
        }

        // 將 data 轉換成 json 格式，再轉換成 base64 字串，再轉換成 hex 字串
        $data = $this->str2hex(base64_encode(json_encode($data)));

        $eth_params = [
            array_merge($request->only(
                ['from', 'to', 'gasPrice', 'gas', 'value', 'nonce', 'condition']
            ), [
                'data' => "0x$data",
            ]),
            $request->get('password')
        ];

        $eth_res = RpcClient::personal_sendTransaction($eth_params);

        $transation = new Transation;
        $transation->eth_hash = $eth_res;
        $transation->order_id = $request->order_id;
        $transation->save();

        $response = [
            'status' => 200,
            'success' => true,
            'data' => $transation,
        ];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transation = Transation::find($id);

        $eth_params = [ $transation->eth_hash ];
        $eth_res = RpcClient::eth_getTransactionByHash($eth_params);
        $eth_res['input_raw'] = json_decode(base64_decode(
          $this->hex2str(
            str_replace('0x', '', $eth_res['input'])
          )
        ));

        $response = [
            'status' => 200,
            'success' => true,
            'data' => $eth_res,
        ];

        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function str2hex($string)
    {
        $string = str_split($string);
        foreach($string as &$char)
            $char = dechex(ord($char));
        return implode('',$string);
    }

    private function hex2str($hex){
        $string='';
        for ($i=0; $i < strlen($hex)-1; $i+=2){
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $string;
    }

    private function parseJsonOrString($data) {
        $json_data = json_decode($data, true);

        return is_null($data)
            ? null
            : is_null($json_data)
                ? $data
                : $json_data;
    }
}
