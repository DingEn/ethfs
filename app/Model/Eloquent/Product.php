<?php

namespace App\Model\Eloquent;

use Illuminate\Database\Eloquent\Model;
use App\Model\Traits\Eloquent\UuidTrait;

class Product extends Model
{
    use UuidTrait;
    public $incrementing = false;
}
